
<?php
/**
 * Template Name: Blog Template
 */
?>

<?php while (have_posts()) : the_post(); ?>
    <?php get_template_part('templates/blog-header-bar'); ?>
    <?php get_template_part('templates/featured-blog-teaser'); ?>
    <?php get_template_part('templates/blog-catagories-menu'); ?>
    <?php get_template_part('templates/blog-posts'); ?>

<!-- <div class="scroll-to-top" href="#index-carousel-main"><div class="arrow"></div>scroll to top</a>-->

<?php endwhile; ?>
