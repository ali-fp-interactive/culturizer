<?php
/**
 * Template Name: Custom Template
 */
?>

<?php while (have_posts()) : the_post(); ?>

  <?php get_template_part('templates/index', 'carousel'); ?>

  <?php get_template_part('templates/content', 'page'); ?>

  <?php get_template_part('templates/two', 'sliders'); ?>

  <?php get_template_part('templates/methode', 'slider'); ?>

  <?php get_template_part('templates/triple', 'slider'); ?>

  <a class="scroll-to-top" href="#index-carousel-main"><div class="arrow"></div>scroll to top</a>

<?php endwhile; ?>
