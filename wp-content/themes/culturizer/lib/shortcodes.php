<?php

// Event Slider
function event_slider_shortcode() {
  ob_start();

  get_template_part('templates/event', 'slider');

  $o = ob_get_contents();
  ob_end_clean();

  return $o;
}
add_shortcode('event_slider', 'event_slider_shortcode');
