<?php

namespace Roots\Sage\Extras;

use Roots\Sage\Setup;

/**
 * Add <body> classes
 */
function body_class($classes) {
  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }

  // Add class if sidebar is active
  if (Setup\display_sidebar()) {
    $classes[] = 'sidebar-primary';
  }

  return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');

/**
 * Clean up the_excerpt()
 */
function excerpt_more() {
  return ' &hellip; <a href="' . get_permalink() . '">' . __('Continued', 'sage') . '</a>';
}
add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');


/*
 * Menu loop
 */
add_filter('wp_nav_menu_objects', __NAMESPACE__ . '\\culturizer_menu_objects', 10, 2);

function culturizer_menu_objects( $items, $args ) {
	foreach( $items as &$item ) {
		$text = get_field('menu_text', $item);
    $linkText = get_field('menu_page_jump_text', $item);
    $linkAnchor = get_field('menu_page_jump_anchor', $item);
		if( $text ) {
			$item->title .= ' <span class="main-menu-text">'.$text.'<span class="main-menu-page-jump"> '.$linkText.'</span></span>';
		}
	}
	return $items;
}


/*
 * Back-End Personalization
 */
 function culturizer_custom_login() {
 echo '<link rel="stylesheet" type="text/css" href="' . get_bloginfo('stylesheet_directory') . '/culturizer-login/culturizer-login-styles.css" />';
 }
 add_action('login_head', __NAMESPACE__ . '\\culturizer_custom_login');


 function culturizer_back_end_logo() {
   echo '<style>
     #wpwrap {
      background: transparent url(http://culturizer.fp-server.com/wp-content/uploads/2017/12/Logo_Platzhalter.png) no-repeat right 25px bottom 35px;
      background-size: 112px 110px;
    }
   </style>';
 }
 add_action('admin_head', __NAMESPACE__ . '\\culturizer_back_end_logo');



 // Removes from admin menu
 add_action( 'admin_menu', __NAMESPACE__ . '\\my_remove_admin_menus' );
 function my_remove_admin_menus() {
     remove_menu_page( 'edit-comments.php' );
 }
 // Removes from post and pages
 add_action('init', __NAMESPACE__ . '\\remove_comment_support', 100);

 function remove_comment_support() {
     remove_post_type_support( 'post', 'comments' );
     remove_post_type_support( 'page', 'comments' );
 }
 // Removes from admin bar
 function mytheme_admin_bar_render() {
     global $wp_admin_bar;
     $wp_admin_bar->remove_menu('comments');
 }
 add_action( 'wp_before_admin_bar_render', __NAMESPACE__ . '\\mytheme_admin_bar_render' );



// add_action( 'admin_menu', __NAMESPACE__ . '\\remove_admin_menus' );
// add_action( 'admin_menu', __NAMESPACE__ . '\\remove_admin_submenus' );
// function remove_admin_menus() {
//     remove_menu_page( 'edit-comments.php' );
//     remove_menu_page( 'link-manager.php' );
//     remove_menu_page( 'tools.php' );
//     remove_menu_page( 'plugins.php' );
//     remove_menu_page( 'users.php' );
//     remove_menu_page( 'options-general.php' );
//     remove_menu_page( 'upload.php' );
//     remove_menu_page( 'edit.php' );
//     remove_menu_page( 'edit.php?post_type=page' );
//     remove_menu_page( 'themes.php' );
// }
// function remove_admin_submenus() {
//     remove_submenu_page( 'themes.php', 'theme-editor.php' );
//     remove_submenu_page( 'themes.php', 'themes.php' );
//     remove_submenu_page( 'edit.php', 'edit-tags.php?taxonomy=post_tag' );
//     remove_submenu_page( 'edit.php', 'edit-tags.php?taxonomy=category' );
//     remove_submenu_page( 'edit.php', 'post-new.php' );
//     remove_submenu_page( 'themes.php', 'nav-menus.php' );
//     remove_submenu_page( 'themes.php', 'widgets.php' );
//     remove_submenu_page( 'themes.php', 'theme-editor.php' );
//     remove_submenu_page( 'plugins.php', 'plugin-editor.php' );
//     remove_submenu_page( 'plugins.php', 'plugin-install.php' );
//     remove_submenu_page( 'users.php', 'users.php' );
//     remove_submenu_page( 'users.php', 'user-new.php' );
//     remove_submenu_page( 'upload.php', 'media-new.php' );
//     remove_submenu_page( 'options-general.php', 'options-writing.php' );
//     remove_submenu_page( 'options-general.php', 'options-discussion.php' );
//     remove_submenu_page( 'options-general.php', 'options-reading.php' );
//     remove_submenu_page( 'options-general.php', 'options-discussion.php' );
//     remove_submenu_page( 'options-general.php', 'options-media.php' );
//     remove_submenu_page( 'options-general.php', 'options-privacy.php' );
//     remove_submenu_page( 'options-general.php', 'options-permalinks.php' );
//     remove_submenu_page( 'index.php', 'update-core.php' );
// }
