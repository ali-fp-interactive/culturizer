
<div class="featured">
<?php $args = array(
        'post_type' => 'post',
        'posts_per_page' => '1',
        'featured' => 'yes'
        );
                //The query
                $the_query = new WP_Query( $args );
                if($the_query->have_posts()) : 
                    while ($the_query->have_posts()) : $the_query->the_post();
            ?>
        <div class="featured-image" style="background-image:url(
                                        <?php
                                        echo wp_get_attachment_image_url(get_field('teaser-image'), 'layout');
                                        ?>)">
        </div>
        <div class="featured-post">
            <div class="post-teaser top">
                <div class="subtitle">
                        <span class="category"> <?php
                                                $categories = get_the_category();
                                                $category_name = $categories[0]->name;
                                                print_r($category_name);
                                                echo(' / ');
                                            ?>/</span><!--
                    --><span class="date"><?php echo get_the_date( );?></span>
                </div>
                <h2><?php the_field('heading');?></h2>
                <p><?php the_field('teaser-text');?></p>
                <a class="more-info" href="<?php
                                                echo get_permalink();
                                                ?>">Details Bitte <?php echo file_get_contents(get_template_directory_uri() . "/dist/images/read-more-arrow.svg"); ?></a>
            </div>
        </div>
        <?php
            endwhile;
        endif;
        ?>
</div>


