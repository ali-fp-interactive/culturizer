<div class="blog">
    <div class="blog-content">
        <?php $args = array(
        'post_type' => 'post',
        'posts_per_page' => '-1',
        );
        //The query
        $the_query = new WP_Query( $args );
        if($the_query->have_posts()) : 
            while ($the_query->have_posts()) : $the_query->the_post();
                ?>
                    <div class="post-teaser <?php 
                                                the_field('layout') ;
                                                echo(' ');
                                                $categories = get_the_category();
                                                $category_slug = $categories[0]->slug;
                                                echo($category_slug);
                                                ?>">
                        <div class="blog-image" style="background-image:url(
                                                    <?php
                                                    echo wp_get_attachment_image_url(get_field('teaser-image'), 'layout');
                                                    ?>)">
                        </div>
                        <div class="text-wrapper">
                            <div class="subtitle">
                                <?php
                                    if( get_field('location') == ''){
                                        ?>
                                            <span class="category"><?php
                                                $categories = get_the_category();
                                                $category_name = $categories[0]->name;
                                                print_r($category_name);
                                                echo(' / ');
                                                /*  echo '<pre>';
                                                print_r(get_the_category( ));
                                                echo '</pre>'; */
                                            ?></span>
                                        <?php
                                    }
                                ?><!--
                            --><span class="date"><?php echo get_the_date( );?></span>
                                <?php
                                    if ( get_field('location') != '') {
                                        ?>
                                            <span class="location"><?php 
                                                                        echo(' / ');
                                                                        the_field('location');
                                                                        ?>
                                            </span>
                                        <?php
                                    }
                                ?>
                            </div>
                            <h2> <?php the_field('heading');?> </h2>
                            <p> <?php the_field('teaser-text');?></p>
                            <a class="more-info" href="<?php
                                                echo get_permalink();
                                                ?>"> Details Bitte <?php echo file_get_contents(get_template_directory_uri() . "/dist/images/read-more-arrow.svg"); ?></a>
                        </div>
                    </div> 
                <?php
            endwhile;
        endif;
        ?>
   
        
</div>






