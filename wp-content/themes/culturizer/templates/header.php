<header class="banner">
  <!-- <div class="container"> -->
    <div class="main-navigation-fixed">
      <a class="brand" href="<?= esc_url(home_url('/')); ?>"><div class="logo"></div></a>
      <!-- language switcher -->
      <ul class="language-switcher">
      <?php
      $languages = icl_get_languages();
      foreach($languages as $language) {
        echo '<li>';
        $language['language_code'];
        if($language['language_code'] != ICL_LANGUAGE_CODE) {
          echo '<a href="' . $language['url'] . '">';
        }
        echo $language['language_code'];
        if($language['language_code'] != ICL_LANGUAGE_CODE) {
          echo '</a>';
        }
        echo '</li>';
      }
      ?>
      </ul>
      <div class="hamburger-wrapper">
        <div class="hamburger">
          <span></span>
          <span></span>
          <span></span>
          <span></span>
        </div>
        <span class="hamburger-menu"><?php _e('MENÜ','culturizer'); ?> </span>
      </div>
      <div class="menu-log-in menu-points-wrapper">
        <a href="https://app.culturizer.de/login">
          <div class="logo-log-in menu-points-inner"></div>
          <span><?php _e('LOG-IN','culturizer'); ?></span>
        </a>
      </div>
      <div class="menu-kontakt menu-points-wrapper">
        <a href="#culturizer-kontakt">
          <div class="logo-kontakt menu-points-inner"></div>
          <span><?php _e('KONTAKT','culturizer'); ?></span>
        </a>
      </div>
    </div>

    <div class="culturizer-main-menu">
      <div class="eks"></div>
      <nav class="nav-primary">
        <?php
        if (has_nav_menu('primary_navigation')) :
          wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav' ]);
        endif;
        ?>
      </nav>
      <div class="culturizer-contact-menu-mobile">
        <ul class="language-switcher-mobile">
          <?php
          $languages = icl_get_languages();
          foreach($languages as $language) {
            echo '<li>';
            $language['language_code'];
            if($language['language_code'] != ICL_LANGUAGE_CODE) {
              echo '<a href="' . $language['url'] . '">';
            }
            echo $language['language_code'];
            if($language['language_code'] != ICL_LANGUAGE_CODE) {
              echo '</a>';
            }
            echo '</li>';
          }
          ?>
        </ul>
        <a class="tel" href="tel:<?php echo get_field('header_tel_href', 'options'); ?>"><?php echo get_field('header_tel_text', 'options'); ?></a>
        <a class="mail" href="mailto:<?php echo get_field('header_mail_href', 'options'); ?>"><?php echo get_field('header_mail_text', 'options'); ?></a>
      </div>
    </div>
    <div class="culturizer-contact-menu">
      <a class="tel" href="tel:<?php echo get_field('header_tel_href', 'options'); ?>"><?php echo get_field('header_tel_text', 'options'); ?></a>
      <a class="mail" href="mailto:<?php echo get_field('header_mail_href', 'options'); ?>"><?php echo get_field('header_mail_text', 'options'); ?></a>
    </div>

  <!-- </div> -->
</header>
