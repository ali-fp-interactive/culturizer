<!--
<div class="featured">
        <div class="featured-image" style="background-image:url()">
        </div>
</div>
<a class="scroll" href="#article"></a> -->
<div class="header-image-post" style="background-image:url(
                                        <?php
                                        echo get_the_post_thumbnail_url();
                                        ?>)">
  <a class="scroll" href="#article"></a>
</div>
<?php while (have_posts()) : the_post(); ?>
  <article id="article" <?php post_class(); ?>>
    <header>
      <h1 class="entry-title"><?php the_title(); ?></h1>
      <?php get_template_part('templates/entry-meta'); ?>
    </header>
    <div class="entry-content">
      <?php the_content(); ?>
    </div>
    <footer>
      <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . __('Pages:', 'sage'), 'after' => '</p></nav>']); ?>
    </footer>
    <?php comments_template('/templates/comments.php'); ?>
  </article>
<?php endwhile; ?>
