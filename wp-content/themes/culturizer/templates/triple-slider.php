<div id="<?php if(ICL_LANGUAGE_CODE == 'de') { echo 'Pakete'; } else { echo 'Packages'; } ?>" class="triple-slider-wrapper">
  <h2 class="detail-slider-headline"><?php echo get_field('paket_main_title', 'option'); ?> </h2>
  <h5 class="detail-slider-headline"><?php echo get_field('paket_main_text', 'option'); ?> </h5>
  <div class="slider-nav">
    <?php
    for ($i = 1; $i <= 1; $i++) {
      if( have_rows('paket_navigation_slider', 'option') ):
        $helper = 0;
        while ( have_rows('paket_navigation_slider', 'option') ) : the_row();
          if( get_row_layout() == 'paket_navigation_slide'):
            $helper++;
            ?>
            <div class="triple-nav triple-nav-<?php echo $helper; ?>">
              <div class="paket-title-wrapper">
                <?php $arr = explode(' ',trim(get_sub_field('paket_title', 'option'))); ?>
                <span class="paket-title-blue"><?php echo $arr[0]; ?></span> <span class="paket-title-black"><?php echo $arr[1]; ?></span>
              </div>
              <div class="triple-nav-img" style="background-image:url(<?php echo get_sub_field('paket_image', 'option')['url']; ?>);"></div>
              <div class="paket-bottom-wrapper">
                <p class="paket-teams-text"><?php echo get_sub_field('paket_teaser_text', 'option'); ?></p>
                <span class="paket-from"><?php echo get_sub_field('from', 'option'); ?></span>
                <?php $arr1 = explode(' ',trim(get_sub_field('number_of_teams', 'option'))); ?>
                <?php
                if ($arr1[0] == '200+') {
                  ?>
                  <span class="paket-teams-blue team-conference"><?php  echo $arr1[0]; ?></span><span class="paket-teams-black"><?php  echo $arr1[1]; ?></span>
                  <?php
                } else {
                  ?>
                  <span class="paket-teams-blue"><?php  echo $arr1[0]; ?></span><span class="paket-teams-black"><?php  echo $arr1[1]; ?></span>
                  <?php
                }
                ?>
                <a class="teams-link" href="<?php echo get_sub_field('button_href', 'option'); ?>"><?php echo get_sub_field('button_text', 'option'); ?></a>
                <a class="teams-excerpt" href="<?php echo get_sub_field('excerpt_href', 'option'); ?>"><?php echo get_sub_field('excerpt_text', 'option'); ?></a>
              </div>
            </div>
            <?php
          endif;
        endwhile;
      else :
      endif;
    }
    ?>
  </div>

  <div class="slider-for" id="triple-slider-info-box">
    <?php
    for ($i = 1; $i <= 1; $i++) {
      if( have_rows('paket_info_box_slider', 'option') ):
        $helper2 = 0;
        while ( have_rows('paket_info_box_slider', 'option') ) : the_row();
          if( get_row_layout() == 'paket_info_box_slide'):
            $helper2++;
            ?>
            <div class="triple-slide triple-slide-<?php echo $helper2; ?>">
              <div class="info-area">
                <div class="paket-box-title-wrapper">
                  <?php $arr = explode(' ',trim(get_sub_field('paket_box_title', 'option'))); ?>
                  <span class="paket-title-blue"><?php echo $arr[0]; ?></span> <span class="paket-title-black"><?php echo $arr[1]; ?></span>
                </div>
                <p class="first-text"><?php echo get_sub_field('paket_box_text_1', 'option'); ?></p>
                <div class="image" style="background-image:url(<?php echo get_sub_field('image', 'option')['url']; ?>);"></div>
                <p class="second-text"><?php echo get_sub_field('paket_box_text_2', 'option'); ?></p>
                <span class="paket-box-slider-title"><?php echo get_sub_field('paket_box_slider_title', 'option'); ?></span>

                <div class="paket-box-slider">
                  <?php
                  if( have_rows('paket_box_slider') ):
                    while ( have_rows('paket_box_slider') ) : the_row();
                    ?>
                    <div class="paket-box-slide">
                      <div class="paket-box-slide-image" style="background-image:url(<?php echo get_sub_field('image', 'option')['url']; ?>);"></div>
                      <p class="paket-box-slide-text"><?php echo get_sub_field('text'); ?></p>
                    </div>
                    <?php
                  endwhile;
                  else :
                  endif;
                  ?>
                </div>

                <span class="paket-box-list-title"><?php echo get_sub_field('paket_box_list_title', 'option'); ?></span>

                <div class="paket-box-list-wrapper">
                <?php
                if( have_rows('paket_box_list') ):
                  while ( have_rows('paket_box_list') ) : the_row();
                  ?>
                  <p class="paket-box-list-li"><?php echo get_sub_field('paket_box_list_li'); ?></p>
                  <?php
                endwhile;
                else :
                endif;
                ?>
                </div>

                <p class="second-text"><?php echo get_sub_field('paket_box_text_3', 'option'); ?></p>

              </div>
            </div>
            <?php
          endif;
        endwhile;
      else :
      endif;
    }
    ?>
  </div>
</div>
