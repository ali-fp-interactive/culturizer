<head>
  <meta charset="utf-8">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="shortcut icon" href="/favicon.png">
  <?php wp_head(); ?>
  <?php
		$host = trim($_SERVER['HTTP_HOST']);
    if($host == "culturizer.de") {
			?>
				<script>
					var gaProperty = 'UA-115750548-1';
					var disableStr = 'ga-disable-' + gaProperty;
					if (document.cookie.indexOf(disableStr + '=true') > -1) {
						window[disableStr] = true;
					}

					function gaOptout() {
						document.cookie = disableStr + '=true; expires=Thu, 31 Dec 2099 23:59:59 UTC; path=/';
						window[disableStr] = true;
						alert("<?php _e('Das Tracking ist jetzt deaktiviert'); ?>");
					}

					window.onload = function() {
						var gaOptouts = document.getElementsByClassName("ga-optout");

						for(var i=0; i < gaOptouts.length; i++) {
							gaOptouts[i].onclick = function() {
								gaOptout();
							}
						}
					}
				</script>
			<?php
			if ( function_exists('cn_cookies_accepted') && cn_cookies_accepted() ) {
				?>
          <!-- Global site tag (gtag.js) - Google Analytics -->
          <script async src="https://www.googletagmanager.com/gtag/js?id=UA-115750548-1"></script>
          <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', 'UA-115750548-1', {'anonymize_ip': true});
		  </script>
				<?php
			}
		}
  ?>
</head>
