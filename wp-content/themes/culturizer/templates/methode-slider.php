<div class="methode-slider-wrapper">
  <h2 class="methode-slider-headline"><?php echo get_field('methode_slider_title', 'option'); ?> </h2>
  <div class="methode-slider">
    <?php
    if( have_rows('methode_slider', 'option') ):
      while ( have_rows('methode_slider', 'option') ) : the_row();
        if( get_row_layout() == 'methode_slide'):

          ?>
          <div class="methode-slide">
              <div class="text-holder">
                <h3><?php echo get_sub_field('title', 'option'); ?></h3>
                <p><?php echo get_sub_field('text', 'option'); ?></p>
              </div>
              <div class="methode-image" style="background-image:url(<?php echo get_sub_field('image', 'option')['url']; ?>);"></div>
            </div>
          <?php

        endif;
      endwhile;
    else :

    endif;
    ?>

  </div>
</div>
