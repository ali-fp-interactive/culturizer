
<div id="index-carousel-main" class="main-carousel">
  <?php
  if( have_rows('main_slider', 'option') ):
    while ( have_rows('main_slider', 'option') ) : the_row();
      if( get_row_layout() == 'main_slide'):

        ?>
        <div class="main-slide">
          <div class="slide-img" style="background-image: url(<?php echo get_sub_field('image', 'option')['url']; ?>);"></div>
          <div class="slider-content-holder">
            <h2><?php echo get_sub_field('title', 'option'); ?></h2>
            <h4><?php echo get_sub_field('text', 'option'); ?></h4>
            <a class="contact" href="<?php echo get_sub_field('button_href', 'option'); ?>"><?php echo get_sub_field('button_text', 'option'); ?></a><br>
            <a class="more-info" href="<?php echo get_sub_field('details_button_href', 'option'); ?>"><?php echo get_sub_field('details_button_text', 'option'); ?></a>
          </div>
        </div>
        <?php

        elseif( get_row_layout() == 'main_video_slide' ):
        ?>
        <div class="main-slide">

          <video class="slide-img  <?php echo get_sub_field('helper_no', 'option'); ?>" id="video-culturizer" poster="https://culturizer.de/<?php echo get_sub_field('video_poster', 'option')['url']; ?>" autoplay muted loop playsinline>
            <source src="<?php echo get_sub_field('mp4_video', 'option'); ?>" type="video/mp4">
            <source src="<?php echo get_sub_field('webm_video', 'option'); ?>" type="video/webm">
          </video>

          <div class="slider-content-holder">
            <h2><?php echo get_sub_field('title', 'option'); ?></h2>
            <h4><?php echo get_sub_field('text', 'option'); ?></h4>
            <a class="contact" href="<?php echo get_sub_field('button_href', 'option'); ?>"><?php echo get_sub_field('button_text', 'option'); ?></a><br>
            <a class="more-info" href="<?php echo get_sub_field('details_button_href', 'option'); ?>"><?php echo get_sub_field('details_button_text', 'option'); ?></a>
          </div>
        </div>
        <?php


      endif;
    endwhile;
  else :

  endif;
  ?>


</div>
<div class="scroll"><a href="#Warum" style="width:26px; height:56px; display:block;"></a></div>
