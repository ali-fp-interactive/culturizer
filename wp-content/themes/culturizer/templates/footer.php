<footer id="culturizer-kontakt" class="content-info">
  <div class="footer-outer">
    <address class="footer-wrapper">
      <h3><?php echo get_field('footer_title', 'options'); ?></h3>
      <span class="beratung"><?php echo get_field('consultations_title', 'options'); ?></span>
      <br>
      <br>
      <a class="footer-phone" href="tel:<?php echo get_field('consultations_tel_href', 'options'); ?>"><?php echo get_field('consultations_tel_text', 'options'); ?></a>
      <br>
      <br>
      <a class="footer-mail" href="mailto:<?php echo get_field('consultations_email_href', 'options'); ?>"><?php echo get_field('consultations_email_text', 'options'); ?></a>
      <?php
        if(ICL_LANGUAGE_CODE == 'de') {
          $comteam = 'https://comteamgroup.com/de/';
        } else {
          $comteam = 'https://comteamgroup.com/en/germany/';
        }
      ?>
      <a href="<?php echo $comteam; ?>" target="_blank"><div class="comteamgroup-logo"></div></a>
    </address>
    <address class="footer-wrapper">
      <span class="address"><?php echo get_field('address_title', 'options'); ?></span>
      <span class="address-text"><?php echo get_field('address_first_row', 'options'); ?></span>
      <span class="address-text"><?php echo get_field('address_second_row', 'options'); ?></span>
      <span class="address-text"><?php echo get_field('address_third_row', 'options'); ?></span>
      <?php
        if(get_field('address_website', 'options') != '') {
          ?><a class="address-text" href="<?php echo get_field('address_website_url', 'options'); ?>" target="_blank"><?php echo get_field('address_website', 'options'); ?></a><?php
        }
      ?>
    </address>
    <div class="two-contact-forms">
      <div class="forms-switcher-wrapper">
        <span class="email-switch">E-MAIL</span>
        <span class="callback-switch">CALLBACK</span>
      </div>
      <?php get_template_part('templates/contact', 'form'); ?>
    </div>

    <nav class="nav-footer">
      <?php
      if (has_nav_menu('footer_navigation')) :
        wp_nav_menu(['theme_location' => 'footer_navigation', 'menu_class' => 'nav' ]);
      endif;
      ?>
    </nav>

  </div>
</footer>
