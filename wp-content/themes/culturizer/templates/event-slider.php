<?php
  if(have_rows('virtual_slider', 'option') || have_rows('digital_slider', 'option') ) {
    ?>
      <div id="detail_slider_headline" class="event-slider-wrapper">

        <h2 class="detail-slider-headline"><?php echo get_field('event_slider_headline', 'option'); ?> </h2>

        <div class="filter-wrapper">
          <?php
            if( have_rows('virtual_slider', 'option') ) {
              ?><span class="filter-virtual">virtuell</span><?php
            }
              if( have_rows('real_slider', 'option') ) {
                ?><span class="filter-real">real</span><?php
              }
          ?>
        </div>

        <div class="two-sliders-events">

          <?php
          if( have_rows('virtual_slider', 'option') ):
            while ( have_rows('virtual_slider', 'option') ) : the_row();
              if( get_row_layout() == 'virtual_slide'):

                ?>
                <div class="slide-virtual slide">
                  <div class="date-location">
                    <?php
                  	 echo get_sub_field('date_location', 'option');
                    ?>
                  </div>
                  <h3>
                    <?php
                  	echo get_sub_field('headline', 'option');
                    ?>
                  </h3>
                  <p>
                    <?php
                    echo get_sub_field('text', 'option');
                    ?>
                  </p>
                  <?php
                    if(get_sub_field('button_text', 'option') != '') {
                      ?>
                        <a href="<?php echo get_sub_field('button_link', 'option'); ?>" target="_blank"><?php echo get_sub_field('button_text', 'option'); ?></a>
                      <?php
                    }
                  ?>
                </div>
                <?php

              endif;
            endwhile;
          else :

          endif;
          ?>

          <?php
          if( have_rows('real_slider', 'option') ):
            while ( have_rows('real_slider', 'option') ) : the_row();
              if( get_row_layout() == 'real_slide'):

                ?>
                <div class="slide-real slide">
                  <div class="date-location">
                    <?php
                  	 echo get_sub_field('date_location', 'option');
                    ?>
                  </div>
                  <h3>
                    <?php
                  	echo get_sub_field('headline', 'option');
                    ?>
                  </h3>
                  <p>
                    <?php
                    echo get_sub_field('text', 'option');
                    ?>
                  </p>
                  <?php
                    if(get_sub_field('button_text', 'option') != '') {
                      ?>
                        <a href="<?php echo get_sub_field('button_link', 'option'); ?>" target="_blank"><?php echo get_sub_field('button_text', 'option'); ?></a>
                      <?php
                    }
                  ?>
                </div>
                <?php

              endif;
            endwhile;
          else :

          endif;
          ?>

        </div>
      </div>
    <?php
  }
?>
