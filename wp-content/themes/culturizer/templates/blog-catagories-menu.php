<div class="filter-wrapper">
<a class="scroll" href="#cat-switch"></a>
    <div class="category-switch" id="cat-switch">
        <?php
            $categories = get_categories();
            foreach ($categories as $category_num) 
            {
                $class = $category_num->slug;
                ?>
                    <div class="cat" data-category="<?php echo $class; ?>">
                        <?php
                            $name = $category_num->name;
                            echo $name;
                        ?>
                    </div>
                <?php
            }
            ?>
    </div>
</div>
<?php
