<div id="detail_slider_headline" class="two-sliders-wrapper">

  <h2 class="detail-slider-headline"><?php echo get_field('detail_slider_headline', 'option'); ?> </h2>

  <div class="filter-wrapper">
    <span class="filter-front-end">Front-End</span>
    <span class="filter-back-end">Back-End</span>
  </div>

  <div class="two-sliders">

    <?php
    if( have_rows('front_end_slider', 'option') ):
      while ( have_rows('front_end_slider', 'option') ) : the_row();
        if( get_row_layout() == 'front_end_slide'):

          ?>
          <div class="slide-front-end slide">
            <div class="two-sliders-img" style="background-image:url(<?php echo get_sub_field('image', 'option')['url']; ?>);"></div>
            <h3>
              <?php
            	echo get_sub_field('headline', 'option');
              ?>
            </h3>
            <p>
              <?php
              echo get_sub_field('text', 'option');
              ?>
            </p>
          </div>
          <?php

        endif;
      endwhile;
    else :

    endif;
    ?>

    <?php
    if( have_rows('back_end_slider', 'option') ):
      while ( have_rows('back_end_slider', 'option') ) : the_row();
        if( get_row_layout() == 'back_end_slide'):

          ?>
          <div class="slide-back-end slide">
            <div class="two-sliders-img" style="background-image:url(<?php echo get_sub_field('image', 'option')['url']; ?>);"></div>
            <h3>
              <?php
            	echo get_sub_field('headline', 'option');
              ?>
            </h3>
            <p>
              <?php
              echo get_sub_field('text', 'option');
              ?>
            </p>
          </div>
          <?php

        endif;
      endwhile;
    else :

    endif;
    ?>

  </div>
</div>
