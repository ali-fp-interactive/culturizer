<?php
/**
 * Template Name: Basic Template
 */
?>

<?php while (have_posts()) : the_post(); ?>

  <?php get_template_part('templates/content', 'page'); ?>

  <a class="scroll-to-top" href="#index-carousel-main"><div class="arrow"></div>scroll to top</a>

<?php endwhile; ?>
