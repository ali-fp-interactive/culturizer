/* ========================================================================
 * DOM-based Routing
 * Based on http://goo.gl/EUTi53 by Paul Irish
 *
 * Only fires on body classes that match. If a body class contains a dash,
 * replace the dash with an underscore when adding it to the object below.
 *
 * .noConflict()
 * The routing is enclosed within an anonymous function so that you can
 * always reference jQuery with $, even when in .noConflict() mode.
 * ======================================================================== */

(function($) {

    // Use this variable to set up the common and page specific functions. If you
    // rename this variable, you will also need to rename the namespace below.
    var Sage = {
        // All pages
        'common': {
            init: function() {
                objectFitImages(); //image scaling javascript activation:>> OFIJS
                // JavaScript to be fired on all pages

                // Scroll To Top
                $(document).scroll(function() {
                    if ($(document).scrollTop() > 500) {
                        $('.scroll-to-top').fadeIn('slow');
                    } else {
                        $('.scroll-to-top').fadeOut('slow');
                    }
                });
                //To stop the search button do stuff with itself and open the search results page
                var windowSize = $(window).width();

                function resizeSearchbar() {
                    if (windowSize < 698) {
                        $(".search-submit").click(function(event) {
                            event.preventDefault();
                            $(".search-field").css({ "display": "block", "width": "11vw", "float": "left" });
                            setTimeout(function() {
                                $(".search-field").css({ "width": 'calc( 100vw - 105px )', "position": "absolute", "right": "71px" });
                                $(".search-submit").css("float", "left");
                                // $(".search-submit").css({ "border-top": "1px solid #2d78bd!important", "border-bottom": "1px solid #2d78bd!important", "border-right": "1px solid #2d78bd!important" });
                                $(".search-submit").css("border-top", "1px solid #2d78bd !important");
                                $(".search-submit").attr("style", "border-bottom:1px solid #2d78bd !important; border-top:1px solid #2d78bd !important; border-right:1px solid #2d78bd !important");
                                $(".heading").css("display", "none");
                            }, 1);
                        });
                    }
                }

                //shave the featured post Text to 135px

                shave('.featured .featured-post .post-teaser.top p', 135);


                resizeSearchbar();
                //Shave.js to trancate long texts and to keep them intact for later use
                //for different media different screen width


                function shaveP() {
                    var teaserCount = 1;
                    $('.blog .blog-content .post-teaser').each(function() {
                        $(this).find('p').css("display", "block");
                        var textWrapper = $(this).find('.text-wrapper');
                        var textWrapperHeight = textWrapper.height();
                        var subtitleWrapper = $(this).find('.subtitle');
                        var subtitleHeight = subtitleWrapper.height();
                        var h2Wrapper = $(this).find('h2');
                        var h2Height = h2Wrapper.height();
                        var pHeight = textWrapperHeight - (subtitleHeight + h2Height + 20);
                        //height is the 
                        //console.log(pHeight, textWrapperHeight, subtitleHeight, h2Height);
                        //console.log('.blog .blog-content .post-teaser:nth-child(' + teaserCount + ') .text-wrapper p');
                        shave('.blog .blog-content .post-teaser:nth-child(' + teaserCount + ') .text-wrapper p', pHeight);
                        //$(this).find('p').clone().children().remove().end().text()

                        if ($(this).find('p').clone().children().remove().end().text() === '') {
                            $(this).find('p').css("display", "none");
                        }


                        /*if ($('.blog .blog-content .post-teaser .long p') == text('...')) {
                            $('.blog .blog-content .post-teaser .long p').hide();

                        }*/

                        /* this was written to shave the heading but the heading cuts off too much and where the largeTeaser layout is the heading gets erased entirely
                        var h2Measure = textWrapperHeight - (pHeight + subtitleHeight);
                        console.log('.blog .blog-content .post-teaser:nth-child(' + headingCount + ') .text-wrapper h2');
                        shave('.blog .blog-content .post-teaser:nth-child(' + headingCount + ') .text-wrapper h2', h2Measure);
*/
                        teaserCount++;

                    });
                }

                setTimeout(function() {
                    shaveP();

                }, 100);
                if (windowSize > 375) {
                    shave('.featured .featured-post .post-teaser.top p', 135);

                } else if (windowSize < 375) {
                    shave('.featured .featured-post .post-teaser.top p', 85);
                }

                /* here is A huge error with the text on featured post because it doesnt refresh tobeing larger text again!!!!!!*/

                $(window).resize(function() {
                    shaveP();

                    if (windowSize > 375) {
                        shave('.featured .featured-post .post-teaser.top p', 135);

                    } else if (windowSize < 375) {
                        shave('.featured .featured-post .post-teaser.top p', 85);
                    }
                });



                //Isotope Layout

                //Filter
                var blogContent = $('.blog-content');
                blogContent.isotope({
                    itemSelector: '.post-teaser',
                    /*work here*/
                    percentPosition: true,
                });

                $('.category-switch .cat').click(function() {

                    if (!$(this).hasClass('active')) {
                        $('.category-switch .cat').removeClass('active');
                        $(this).addClass('active');
                        var this_filter = $(this).attr('data-category');
                        console.log(this_filter);

                        blogContent.isotope({
                            filter: '.' + this_filter,
                        });
                    } else {
                        $('.category-switch .cat').removeClass('active');
                        blogContent.isotope({
                            filter: '*'
                        });
                    }
                });


                //Parallax
                if ($(window).width() >= 768) {

                    $(document).scroll(function() {
                        var scrollTop = $(window).scrollTop();
                        var screenCenter = $(window).height() / 2;

                        var elementImage = $('.vertical-parallax-image');
                        $(elementImage).each(function() {
                            var elementOffsetImage = $(this).offset().top;
                            var elementHeightImage = $(this).height() / 2;
                            var elementCenterImage = (elementOffsetImage - scrollTop - screenCenter + elementHeightImage) / 10;
                            $(this).css({
                                'transform': 'translateY(' + elementCenterImage + 'px)'
                            });
                        });

                        var elementText = $('body.home .vertical-parallax-text');
                        $(elementText).each(function() {
                            var elementOffsetText = $(this).offset().top;
                            var elementHeightText = $(this).height() / 2;
                            var elementCenterText = 0 - ((elementOffsetText - scrollTop - screenCenter + elementHeightText) / 10);
                            $(this).css({
                                'transform': 'translateY(' + elementCenterText + 'px)'
                            });
                        });
                    });
                }

                // Main Navgation
                $('.eks, li a').click(function() {
                    $('.hamburger').removeClass('open');
                    $('.culturizer-main-menu').removeClass('menu-show');
                    $('.culturizer-contact-menu').removeClass('menu-show');
                });

                $('.hamburger, .hamburger-menu').click(function() {
                    if ($('.culturizer-contact-menu').hasClass('menu-show') && !$('.culturizer-main-menu').hasClass('menu-show')) {
                        $('.hamburger').toggleClass('open');
                        $('.culturizer-main-menu').toggleClass('menu-show');
                    } else if ($('.culturizer-contact-menu').hasClass('menu-show') && $('.culturizer-main-menu').hasClass('menu-show')) {
                        $('.hamburger').toggleClass('open');
                        $('.culturizer-main-menu').toggleClass('menu-show');
                        $('.culturizer-contact-menu').toggleClass('menu-show');
                    } else if (!$('.culturizer-contact-menu').hasClass('menu-show') && !$('.culturizer-main-menu').hasClass('menu-show')) {
                        $('.hamburger').toggleClass('open');
                        $('.culturizer-main-menu').toggleClass('menu-show');
                        $('.culturizer-contact-menu').toggleClass('menu-show');
                    } else {
                        $('.hamburger').toggleClass('open');
                        $('.culturizer-main-menu').toggleClass('menu-show');
                    }
                });

                $('.menu-kontakt').click(function() {
                    if ($('.culturizer-contact-menu').hasClass('menu-show') && $('.culturizer-main-menu').hasClass('menu-show')) {
                        $('.hamburger').toggleClass('open');
                        $('.culturizer-main-menu').toggleClass('menu-show');
                        $('.culturizer-contact-menu').toggleClass('menu-show');
                    } else {
                        $('.culturizer-contact-menu').toggleClass('menu-show');
                    }
                });


                $('.triple-slide-1 .info-area').addClass('toggle-first-product');
                $('.triple-nav-1').click(function() {
                    $('.triple-slide-2 .info-area').removeClass('toggle-second-product');
                    $('.triple-slide-3 .info-area').removeClass('toggle-third-product');
                    $('.triple-slide-1 .info-area').addClass('toggle-first-product');
                });
                $('.triple-nav-2').click(function() {
                    $('.triple-slide-1 .info-area').removeClass('toggle-first-product');
                    $('.triple-slide-3 .info-area').removeClass('toggle-third-product');
                    $('.triple-slide-2 .info-area').addClass('toggle-second-product');
                });
                $('.triple-nav-3').click(function() {
                    $('.triple-slide-1 .info-area').removeClass('toggle-first-product');
                    $('.triple-slide-2 .info-area').removeClass('toggle-second-product');
                    $('.triple-slide-3 .info-area').addClass('toggle-third-product');
                });


                // Sets Style for Testimonials p tag
                var allChildTitles = $('.testimonial-title');
                $('p').find(allChildTitles).parent().css('line-height', '16px');



                // Select all links with hashes
                $('a[href*="#"]')
                    // Remove links that don't actually link to anything
                    .not('[href="#"]')
                    .not('[href="#0"]')
                    .click(function(event) {
                        // On-page links
                        if (location.pathname.replace(/^\//, '') === this.pathname.replace(/^\//, '') && location.hostname === this.hostname) {
                            // Figure out element to scroll to
                            var target = $(this.hash);
                            target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                            // Does a scroll target exist?
                            if (target.length) {
                                // Only prevent default if animation is actually gonna happen
                                event.preventDefault();
                                $('html, body').animate({
                                    scrollTop: target.offset().top
                                }, 1000, function() {
                                    // Callback after animation
                                    // Must change focus!
                                    var $target = $(target);
                                    $target.focus();
                                    if ($target.is(":focus")) { // Checking if the target was focused
                                        return false;
                                    } else {
                                        $target.attr('tabindex', '-1'); // Adding tabindex for elements not focusable
                                        $target.focus(); // Set focus again
                                    }
                                });
                            }
                        }
                    });


                // Kontakt stuff
                $('.contact-form-email').addClass('show-form').removeClass('remove-form');
                $('.contact-form-callback').addClass('remove-form').removeClass('show-form');

                $('.email-switch').click(function() {
                    $(this).addClass('switch-active').removeClass('switch-inactive');
                    $('.callback-switch').addClass('switch-inactive').removeClass('switch-active');
                    $('.contact-form-email').addClass('show-form').removeClass('remove-form');
                    $('.contact-form-callback').addClass('remove-form').removeClass('show-form');
                });

                $('.callback-switch').click(function() {
                    $(this).addClass('switch-active').removeClass("switch-inactive");
                    $('.email-switch').addClass('switch-inactive').removeClass('switch-active');
                    $('.contact-form-callback').addClass('show-form').removeClass('remove-form');
                    $('.contact-form-email').addClass('remove-form').removeClass('show-form');
                });



            },
            finalize: function() {
                // JavaScript to be fired on all pages, after page specific JS is fired
            }
        },
        // Home page
        'home': {
            init: function() {
                // JavaScript to be fired on the home page


                setTimeout(function() {
                    $('#video-culturizer')[0].play();
                }, 150);
                setTimeout(function() {
                    $('.slide-two')[0].play();
                }, 150);


                // Sets Index Page Carousel
                $('.main-carousel').slick({
                    dots: true,
                    fade: true,
                    cssEase: 'linear'
                });


                // Sets PACKAGE sliders
                $('.slider-for').slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: false,
                    fade: true,
                    infinite: true,
                    speed: 700,
                    asNavFor: '.slider-nav',
                });
                $('.slider-nav').slick({
                    slidesToShow: 3,
                    slidesToScroll: 3,
                    speed: 700,
                    asNavFor: '.slider-for',
                    centerPadding: '0',
                    centerMode: false,
                    focusOnSelect: true,
                    infinite: false,
                });


                // Sets INFO BOX sliders
                $('.paket-box-slider').slick({
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    arrows: true,
                    adaptiveHeight: true,
                    infinite: true,
                    speed: 700,
                    cssEase: 'linear',
                    fade: true
                });


                // Sets TWO FILTER sliders
                var $carouselEvents = $('.two-sliders-events').slick({
                    autoplay: false,
                    autoplaySpeed: 1000,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: false,
                    cssEase: 'linear',
                    fade: true,
                    slickFilter: ':has(.slide-front-end)'
                });
                var $carousel = $('.two-sliders').slick({
                    autoplay: false,
                    autoplaySpeed: 1000,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    infinite: true,
                    cssEase: 'linear',
                    fade: true,
                    slickFilter: ':has(.slide-front-end)'
                });

                // Trigger Front End Detail Slider On Page load
                setTimeout(function() {
                    $('.filter-virtual').trigger('click');
                    $('.filter-front-end').trigger('click');
                }, 2000);

                // Buttons
                $('.filter-virtual').click(function(e) {
                    $carouselEvents.slick('slickUnfilter');
                    $carouselEvents.slick('slickFilter', ':has(.slide-virtual)');
                    $(this).removeClass('button-inactive');
                    $(this).addClass('button-active');
                    $('.filter-real').removeClass('button-active');
                    $('.filter-real').addClass('button-inactive');
                });
                $('.filter-real').click(function(e) {
                    $carouselEvents.slick('slickUnfilter');
                    $carouselEvents.slick('slickFilter', ':has(.slide-real)');
                    $(this).removeClass('button-inactive');
                    $(this).addClass('button-active');
                    $('.filter-virtual').removeClass('button-active');
                    $('.filter-virtual').addClass('button-inactive');
                });
                $('.filter-front-end').click(function(e) {
                    $carousel.slick('slickUnfilter');
                    $carousel.slick('slickFilter', ':has(.slide-front-end)');
                    $(this).removeClass('button-inactive');
                    $(this).addClass('button-active');
                    $('.filter-back-end').removeClass('button-active');
                    $('.filter-back-end').addClass('button-inactive');
                });
                $('.filter-back-end').click(function(e) {
                    $carousel.slick('slickUnfilter');
                    $carousel.slick('slickFilter', ':has(.slide-back-end)');
                    $(this).removeClass('button-inactive');
                    $(this).addClass('button-active');
                    $('.filter-front-end').removeClass('button-active');
                    $('.filter-front-end').addClass('button-inactive');
                });


                // Sets Methode Slider
                $('.methode-slider').slick({
                    dots: false,
                    fade: true,
                    adaptiveHeight: true,
                    cssEase: 'linear'
                });


            },
            finalize: function() {
                // JavaScript to be fired on the home page, after the init JS
            }
        },
        // About us page, note the change from about-us to about_us.
        'about_us': {
            init: function() {
                // JavaScript to be fired on the about us page
            }
        }
    };

    // The routing fires all common scripts, followed by the page specific scripts.
    // Add additional events for more control over timing e.g. a finalize event
    var UTIL = {
        fire: function(func, funcname, args) {
            var fire;
            var namespace = Sage;
            funcname = (funcname === undefined) ? 'init' : funcname;
            fire = func !== '';
            fire = fire && namespace[func];
            fire = fire && typeof namespace[func][funcname] === 'function';

            if (fire) {
                namespace[func][funcname](args);
            }
        },
        loadEvents: function() {
            // Fire common init JS
            UTIL.fire('common');

            // Fire page-specific init JS, and then finalize JS
            $.each(document.body.className.replace(/-/g, '_').split(/\s+/), function(i, classnm) {
                UTIL.fire(classnm);
                UTIL.fire(classnm, 'finalize');
            });

            // Fire common finalize JS
            UTIL.fire('common', 'finalize');
        }
    };

    // Load Events
    $(document).ready(UTIL.loadEvents);

})(jQuery); // Fully reference jQuery after this point.