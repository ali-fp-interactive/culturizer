<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
 //Added by WP-Cache Manager
define('DB_NAME', 'culturizer');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '&JTMq#N&,o_#{s~2l]K_bwD2h)*?zo;l4[(-)f:iK$bOIMd:]N4]^IX-$tpcrN{m');
define('SECURE_AUTH_KEY',  'BKw2E^VXa%R2=GfXP?tLx?H/{QDK8Es.$5MB<m}|Y1Qlh0*H? oaaee|x@9%/SHU');
define('LOGGED_IN_KEY',    'M7^EP;Y |Oo-cbx5wo BLc@T1$p&q`]eg^ a{U041K$!xBlH;H#`p_#uBKS5}A<<');
define('NONCE_KEY',        'k,2n~q%h9~uOZd49<rI]mfMg$Jh@K a=5Kf$*@T|Oh6]1LFv255P/S+2duxN]I]N');
define('AUTH_SALT',        'Sglutu$}p%C[Gu}#seF,}+i)v2L{SOS;u][OGrTW^I>C6K9sI,qSaWy%<g_!,Jfb');
define('SECURE_AUTH_SALT', '&akN`H;1|-rw~^sJMq/k`V#KBtr7Rk y,QNm&guQEw3ZEk%.0,zz2)$X?vh/$<L6');
define('LOGGED_IN_SALT',   '?jOYF]EnMnXOcsDzL}LB+tpvMleGm{n2Rg:uR;XYXod3_<UVl!Q+$w4QX:nTX8#V');
define('NONCE_SALT',       'CGAM+.8EK =N3:{S[-v[Y7=l{+o7zGf.nwD2XL[fBT.xg/n_)ed$fF@:Jnm(?r/3');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

// ========================
// Custom Content Directory
// ========================
define( 'WP_CONTENT_DIR', dirname( __FILE__ ) . '/wp-content' );
define( 'WP_CONTENT_URL', 'http://' . $_SERVER['HTTP_HOST'] . '/wp-content' );


/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
    define( 'ABSPATH', dirname( __FILE__ ) . '/wp/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
